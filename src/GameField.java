import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Random;

public class GameField extends JPanel implements ActionListener {

    private final int SIZE = 600;
    private final int DOT_SIZE = 30;
    private final int ALL_DOTS = 400;
    private Image dot;
    private Image apple;
    private int appleX;
    private int appleY;
    private int[] x = new int[ALL_DOTS];
    private int[] y = new int[ALL_DOTS];
    private int dots;
    private boolean left = false;
    private boolean right = true;
    private boolean up = false;
    private boolean down = false;
    private boolean inGame = true;

    public GameField() {
        loadImages();
        initGame();
        setBackground(Color.GRAY);
        addKeyListener(new FieldKeyListener());
        setFocusable(true);
    }

    public void initGame() {
        dots = 3;
        for (int i=0; i<dots; i++) {
            x[i] = 90 - (i * DOT_SIZE);
            y[i] = 90;
        }
        Timer timer = new Timer(250, this);
        timer.start();
        createApple();
    }

    public void createApple() {
        appleX = new Random().nextInt(20) * DOT_SIZE;
        appleY = new Random().nextInt(20) * DOT_SIZE;
    }

    public void loadImages() {
        ImageIcon iiapple = new ImageIcon("apple.png");
        apple = iiapple.getImage();

        ImageIcon iidot = new ImageIcon("dot.png");
        dot = iidot.getImage();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(inGame) {
            g.drawImage(apple, appleX, appleY, this);
            for(int i=0; i<dots; i++) {
                g.drawImage(dot, x[i], y[i], this);
            }

            for (int i=0; i<=SIZE; i+=DOT_SIZE) {
                g.drawLine(0, i, SIZE, i);
                g.drawLine(i, 0, i, SIZE);
            }
        }
    }

    public void move() {
        for(int i=dots; i>0; i--) {
            x[i] = x[i-1];
            y[i] = y[i-1];
        }

        if(left) {
            x[0] -= DOT_SIZE;
        }

        if(right) {
            x[0] += DOT_SIZE;
        }

        if(up) {
            y[0] -= DOT_SIZE;
        }

        if(down) {
            y[0] += DOT_SIZE;
        }
    }

    public void checkApple() {
        if(appleX == x[0] && appleY == y[0]) {
            dots++;
            createApple();
        }
    }

    public void checkCollisions() {
        for(int i=dots; i>0; i--) {
            if(i > 4 && x[0] == x[i] && y[0] == y[i]) {
                inGame = false;
            }

            if(x[0] > SIZE) {
                x[0] = 0;
            }

            if(x[0] < 0) {
                x[0] = SIZE;
            }

            if(y[0] > SIZE) {
                y[0] = 0;
            }

            if(y[0] < 0) {
                y[0] = SIZE;
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(inGame) {
            move();
            checkApple();
            checkCollisions();
        }
        repaint();
    }

    class FieldKeyListener extends KeyAdapter {
        @Override
        public void keyPressed(KeyEvent e) {
            super.keyPressed(e);
            int key = e.getKeyCode();

            if(key == KeyEvent.VK_LEFT && ! right) {
                left = true;
                up = false;
                down = false;
            }

            if(key == KeyEvent.VK_RIGHT && ! left) {
                right = true;
                up = false;
                down = false;
            }

            if(key == KeyEvent.VK_UP && ! down) {
                up = true;
                left = false;
                right = false;
            }

            if(key == KeyEvent.VK_DOWN && ! up) {
                down = true;
                left = false;
                right = false;
            }

        }
    }
}
