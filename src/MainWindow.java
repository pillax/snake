import javax.swing.*;

public class MainWindow extends JFrame {

    public MainWindow() {
        setTitle("KK Snake Test");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(615, 640);
        setLocation(400, 400);
        add(new GameField());
        setVisible(true);
    }

    public static void main(String[] args) {
        MainWindow mw = new MainWindow();
    }
}
